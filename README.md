#InstaClone BackEND

Criado no padrão de estrutura MVC, esse é o backend Clone Instagram entregando dados em forma de API rest para os front ends WEB (https://gitlab.com/aszarth/instaclone_frontend) / MOBILE (https://gitlab.com/aszarth/instaclone_mobile).

Os dados estão sendo salvos em MYSQL na versão opensource MarinaDB.
Para facilitar as consultas são feitas atraves de classes usando sequelize como ORM.

As imagens upadas pelo {multer}, são convertidas para JPG e tratadas para ficarem leves no backend com o uso do {sharp} 

informações sobre a arquitetura do sistema em:
diario_arquitetura

informações sobre dependencias baixadas e como rodar o projeto em:
diario_npm


--- INFO ---
tem que criar um arquivo em src/config
database.js com as configurações de conexão:
[code]
module.exports = {
  username: 'usr',
  password: 'pss',
  database: 'dbname',
  host: '127.0.0.1',
  dialect: 'mysql',
}
[/code]