const {Post} = require('../models/');
const sharp = require('sharp');
const path = require('path');
const fs = require('fs');

module.exports = {
  // listar todos os posts
  async showall(req, res) {
    const posts = await Post.findAll({
      order: [['createdAt', 'DESC']],
    });
    return res.json(posts);
  },

  // upload de imgs
  // no ismonia usar multipart-form data pq tem img, outros casos é JSON msm
  async create(req, res) {
    // console.log(req.body);
    const {author, place, description, hashtags} = req.body;
    const {filename: image} = req.file;
    // separar o nome da extensão
    const [name, ext] = image.split('.');
    const fileName = `${name}.jpg`;
    // tratar img pra ficar mais leve no front
    await sharp(req.file.path)
        .resize(500)
        .jpeg({quality: 70})
        .toFile(path.resolve(req.file.destination, 'resized', fileName));
    // apagar a antiga
    fs.unlinkSync(req.file.path);

    const post = await Post.create({
      author,
      place,
      description,
      hashtags,
      image: fileName,
    });
    req.io.emit('post', post);
    res.json(post);
  },
};
