const {User} = require('../models/');

const Sequelize = require('sequelize');
const op = Sequelize.Op; // operador OR

const crypto = require('crypto');
const {hash_code: passHashCode} = require('../config/hash_pass.json');

const jwt = require('jsonwebtoken');
const {secret: authHashCode} = require('../config/auth.json');
const jwt_decode = require('jwt-decode');

// token (userid+hashconfig+timestamp pra gerar um valor unico)
function generateToken(params = {}) {
  return jwt.sign(params, authHashCode, {
    expiresIn: 86400, // secs (1dia)
  });
}

module.exports = {
  // usando BODY
  async showall(req, res) {
    const aux = await User.findAll({
      order: [['createdAt', 'DESC']],
    });
    return res.json(aux);
  },

  async register(req, res) {
    const {username, mail, password} = req.body;
    // checar se já tem user name/email registrado
    const registeredAux = await User.findOne({
      where: {
        [op.or]: [{username}, {mail}],
      },
    });
    if (registeredAux) {
      return res.status(400).send({error: 'UserName/Mail already exist'});
    }
    // nao tem, entao
    // encrypta senha
    const hash = crypto
        .createHash('sha256')
        .update(password + passHashCode) // concatenation
        .digest('hex') // get hash in hex format
        .toUpperCase(); // letras em maiusculo
    // reg
    const usr = await User.create({
      username,
      mail,
      password: hash,
    });
    // pra api não retornar a senha
    usr.password = undefined;
    res.json({
      usr,
      token: generateToken({id: usr.id}),
    });
  },

  // usando PARAMS
  async findone(req, res) {
    const {username} = req.params;
    const aux = await User.findOne({
      where: {username},
    });
    return res.json(aux);
  },

  async delete(req, res) {
    const {id} = req.params;
    const aux = await User.destroy({
      where: {
        id,
      },
    });
    return res.json(aux);
  },

  // usando PARAMS + BODY
  async edit(req, res) {
    const {username} = req.params;
    const {newMail} = req.body;
    const aux = await User.update(
        {
          mail: newMail,
        },
        {
          where: {
            username,
          },
        }
    );
    return res.json(aux);
  },

  // JWT
  // usando BODY
  async findbytoken(req, res) {
    const {token} = req.body;

    const decoded = jwt_decode(token);
    const id = decoded.id;

    const aux = await User.findOne({
      where: {id},
    });
    return res.json(aux);
  },

  async auth(req, res) {
    const {mail, password} = req.body;
    // checar se já tem user email registrado
    const usr = await User.findOne({
      where: {
        mail,
      },
    });
    if (!usr) {
      return res.status(400).send({error: 'User not found'});
    }
    // checar senha encryptada
    const hash = crypto
        .createHash('sha256')
        .update(password + passHashCode) // concatenation
        .digest('hex') // get hash in hex format
        .toUpperCase(); // letras em maiusculo
    if (hash !== usr.password) {
      return res.status(400).send({error: 'Invalid password'});
    }
    // pra api não retornar a senha pro cara
    usr.password = undefined;
    res.send({
      usr,
      token: generateToken({id: usr.id}),
    });
  },
};
