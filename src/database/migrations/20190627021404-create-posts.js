'use strict';

module.exports = {
  up: (queryInterface, DataTypes) => {
    return queryInterface.createTable('Posts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      author: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      place: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      description: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      hashtags: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      image: {
        allowNull: false,
        type: DataTypes.STRING(128),
      },
      likes: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Posts');
  }
};
