const express = require('express');
const path = require('path');
const cors = require('cors');

const hostname = 'localhost';
const port = 3333;

const app = express();

// protolo http
const server = require('http').Server(app);
// tambem conexoes via websocket
const io = require('socket.io')(server);

// criando o proprio middleware pra usar o IO em todas as outras janelas
app.use((req, res, next) => {
  req.io = io;
  next(); // garantir que o codigo nao fique preso no /\
});

// atalhos para links, como:
// http://localhost:3333/files/foto1.jpg
app.use(
    '/files',
    express.static(path.resolve(__dirname, '..', 'tmp', 'uploads', 'resized'))
);

// permitir que o nosso backend seja acessivel pelo react, msm que em dominios diferentes
// da pra colocar dominio q pode acessar e tal
app.use(cors());

// conseguir usar os jsons nas rotas
app.use(express.json());
app.use(require('./routes'));

server.listen(port);

console.log(`Server running at http://${hostname}:${port}/`);
