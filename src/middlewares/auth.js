const jwt = require('jsonwebtoken');
const {secret: authHashConfig} = require('../config/auth.json');

module.exports = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) return res.status(401).send({error: 'No token provided'});
  const parts = authHeader.split(' '); // separar pelos espaços p pegar o barrer
  // dividiu e tem 2 partes?
  if (!parts.length === 2) {
    return res.status(401).send({error: 'Token error'});
  }
  const [scheme, token] = parts;
  // verificar se tem bearer escrito
  // regex, pq pode ter mais q bearer
  // começa com bearer?
  // [!] negação, [/] começa regex, [^] inicio, var, [$] final, i case sensetive
  if (!/^Bearer$/i.test(scheme)) {
    return res.status(401).send({error: 'Token malformatted'});
  }
  // agora a verificação pesada, essas de cima são pra deixar o back suave
  jwt.verify(token, authHashConfig, (err, decoded) => {
    if (err) return res.status(401).send({error: 'Token invalid'});
    req.userId = decoded.id;
    // ta habilitado pra ir pro controller
    return next();
  });
};
