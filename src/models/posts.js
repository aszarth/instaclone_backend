module.exports = (sequelize, DataTypes) => {
  const Post = sequelize.define('Post', {
    author: DataTypes.STRING(128),
    place: DataTypes.STRING(128),
    description: DataTypes.STRING(128),
    hashtags: DataTypes.STRING(128),
    image: DataTypes.STRING(128),
    likes: {
      type: DataTypes.INTEGER(32),
      defaultValue: 0,
    },
  });
  return Post;
};
