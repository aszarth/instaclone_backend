module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING(128),
    mail: DataTypes.STRING(128),
    password: DataTypes.STRING(128),
  });

  return User;
};
