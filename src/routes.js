const express = require('express');
const multer = require('multer');
const uploadConfig = require('./config/upload');

const UserController = require('./controllers/UserController');
const PostController = require('./controllers/PostController');
const LikeController = require('./controllers/LikeController');

// JWT
const authMiddlware = require('./middlewares/auth');

const routes = new express.Router();
const upload = multer(uploadConfig);

//
routes.post('/users/authenticate', UserController.auth);
routes.post('/users', UserController.register); // Criar
// JWT
routes.get('/users', authMiddlware, UserController.showall); // Listar todos
routes.post('/users/findbytoken', UserController.findbytoken); // Buscar Pelo Token

//
routes.get('/users/:username', UserController.findone); // Buscar
routes.put('/users/:username', UserController.edit); // Editar
routes.delete('/users/:id', UserController.delete); // Deletar

// lista todos os posts
routes.get('/posts', PostController.showall);
// postar, com upload
routes.post('/posts', upload.single('image'), PostController.create);
// adicionar um like
routes.post('/posts/:id/like', LikeController.store);

// web test
// http://localhost:3333/?name=nome
routes.get('/', (req, res) => {
  return res.send(`Hello ${req.query.username}`);
});

module.exports = routes;
